# /!\ Deprecated

This repository has been moved into
[tezos/tezos](https://gitlab.com/tezos/tezos) and is deprecated.

# Tezos rust libraries (and dependencies)
This repository contains all the rust libraries used in the codebase of tezos/tezos as well as all their dependencies vendored. The purpose is to make a self-contained archive which allows a compilation inside `opam` "sandbox".

## How to change something
 - Add or update libraries
 - Complete or adapt the list in `Cargo.toml`
 - Refresh `Cargo.lock` with `cargo update`
 - Run `cargo vendor` to regenerate `vendor/`
 - Commit everything

## Dependencies tweaks

- librustzcash:
  - change lib path of Cargo.toml to map this repository structure
  - deactivate Orchard parameters loading
- wasmer-3.3.0:
  - Strip unused workspace declaration
  - Fix C API build.rs (https://github.com/wasmerio/wasmer/issues/3186)
