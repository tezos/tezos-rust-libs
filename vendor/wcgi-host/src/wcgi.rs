use std::collections::HashMap;

use http::Request;
use tokio::io::{AsyncBufRead, AsyncBufReadExt};

use crate::CgiError;

pub(crate) async fn extract_response_header(
    stdout: &mut (impl AsyncBufRead + Unpin),
) -> Result<http::response::Parts, CgiError> {
    let mut response_header_raw = Vec::new();

    stdout
        .read_until(b'\n', &mut response_header_raw)
        .await
        .map_err(CgiError::StdoutRead)?;

    let header = response_header_raw
        .strip_suffix(b"\n")
        .unwrap_or(&response_header_raw);

    let (parts, _body) = match wcgi::convert::deserialize_response_header_v1_json(header) {
        Ok(res) => res.into_parts(),
        Err(error) => {
            return Err(CgiError::MalformedWcgiHeader {
                error,
                header: String::from_utf8_lossy(header).into_owned(),
            });
        }
    };

    Ok(parts)
}

pub(crate) fn prepare_environment_variables(
    parts: http::request::Parts,
    env: &mut HashMap<String, String>,
) {
    let request = Request::from_parts(parts, None);
    let header = wcgi::convert::serialize_request_header_v1_json(request).unwrap();
    env.insert(wcgi::convert::ENV_VAR_REQUEST_JSON_V1.to_string(), header);
}
