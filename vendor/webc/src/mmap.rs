use std::{fs::File, ops::Deref};

#[derive(Debug)]
pub(crate) struct SharedMmapState {
    mmap: memmap2::Mmap,
    // Note: We the file needs to outlive its memory map.
    #[allow(dead_code)]
    file: File,
}

impl SharedMmapState {
    pub(crate) fn new(file: File) -> Result<Self, std::io::Error> {
        let mmap = unsafe { memmap2::MmapOptions::new().map(&file)? };
        Ok(SharedMmapState { mmap, file })
    }
}

impl Deref for SharedMmapState {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        &self.mmap
    }
}
