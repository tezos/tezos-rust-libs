use std::hash::Hasher;
use std::{borrow::Cow, hash::Hash};

/// Owned bytes that can be cheaply copied and shared around.
///
/// When the `mmap` flag is enabled, this
#[derive(Debug, Clone, Eq, Hash, PartialOrd, Ord)]
pub struct SharedBytes {
    repr: Repr,
}

impl SharedBytes {
    #[cfg(feature = "mmap")]
    pub(crate) fn mapped(
        mmap: std::sync::Arc<crate::SharedMmapState>,
        range: std::ops::Range<usize>,
    ) -> Self {
        SharedBytes {
            repr: Repr::Mapped { mmap, range },
        }
    }

    /// Get a reference to the underlying bytes.
    pub fn as_slice(&self) -> &[u8] {
        self.repr.as_slice()
    }
}

impl AsRef<[u8]> for SharedBytes {
    fn as_ref(&self) -> &[u8] {
        self
    }
}

impl From<bytes::Bytes> for SharedBytes {
    fn from(value: bytes::Bytes) -> Self {
        SharedBytes {
            repr: Repr::Bytes(value),
        }
    }
}

impl From<SharedBytes> for bytes::Bytes {
    fn from(value: SharedBytes) -> Self {
        match value.repr {
            Repr::Bytes(b) => b,
            #[cfg(feature = "mmap")]
            Repr::Mapped { .. } => value.to_vec().into(),
        }
    }
}

impl From<&'static [u8]> for SharedBytes {
    fn from(value: &'static [u8]) -> Self {
        SharedBytes {
            repr: Repr::Bytes(value.into()),
        }
    }
}

impl From<Vec<u8>> for SharedBytes {
    fn from(value: Vec<u8>) -> Self {
        SharedBytes {
            repr: Repr::Bytes(value.into()),
        }
    }
}

impl From<SharedBytes> for Vec<u8> {
    fn from(value: SharedBytes) -> Vec<u8> {
        match value.repr {
            Repr::Bytes(b) => b.into(),
            #[cfg(feature = "mmap")]
            Repr::Mapped { .. } => value.to_vec(),
        }
    }
}

impl PartialEq<[u8]> for SharedBytes {
    fn eq(&self, other: &[u8]) -> bool {
        self.repr.as_slice().eq(other)
    }
}

impl<B> PartialEq<B> for SharedBytes
where
    B: AsRef<[u8]>,
{
    fn eq(&self, other: &B) -> bool {
        self == other.as_ref()
    }
}

impl<const N: usize> PartialEq<SharedBytes> for [u8; N] {
    fn eq(&self, other: &SharedBytes) -> bool {
        other == self
    }
}

impl PartialEq<SharedBytes> for [u8] {
    fn eq(&self, other: &SharedBytes) -> bool {
        other == self
    }
}

impl PartialEq<SharedBytes> for &[u8] {
    fn eq(&self, other: &SharedBytes) -> bool {
        other == self
    }
}

impl PartialEq<SharedBytes> for Vec<u8> {
    fn eq(&self, other: &SharedBytes) -> bool {
        other == self
    }
}

impl PartialEq<SharedBytes> for bytes::Bytes {
    fn eq(&self, other: &SharedBytes) -> bool {
        other == self
    }
}

impl PartialEq<SharedBytes> for Cow<'_, [u8]> {
    fn eq(&self, other: &SharedBytes) -> bool {
        other == self
    }
}

impl std::ops::Deref for SharedBytes {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        &self.repr
    }
}

#[derive(Debug, Clone)]
enum Repr {
    Bytes(bytes::Bytes),
    #[cfg(feature = "mmap")]
    Mapped {
        mmap: std::sync::Arc<crate::SharedMmapState>,
        range: std::ops::Range<usize>,
    },
}

impl Repr {
    fn as_slice(&self) -> &[u8] {
        match self {
            Repr::Bytes(b) => b,
            #[cfg(feature = "mmap")]
            Repr::Mapped { mmap, range } => &mmap[range.clone()],
        }
    }
}

impl Hash for Repr {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.as_slice().hash(state);
    }
}

impl PartialEq for Repr {
    fn eq(&self, other: &Self) -> bool {
        self.as_slice() == other.as_slice()
    }
}
impl Eq for Repr {}

impl PartialOrd for Repr {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.as_slice().partial_cmp(other.as_slice())
    }
}

impl Ord for Repr {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.as_slice().cmp(other.as_slice())
    }
}

impl std::ops::Deref for Repr {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        self.as_slice()
    }
}
