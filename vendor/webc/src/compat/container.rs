use std::{
    borrow::Cow,
    collections::BTreeMap,
    fmt::Debug,
    fs::File,
    io::{Read, Seek},
    path::Path,
    sync::Arc,
};

use bytes::Bytes;

use crate::{
    compat::{SharedBytes, Volume},
    v1::WebC,
    Version,
};

/// A version-agnostic read-only WEBC container.
///
/// A `Container` provides a high-level interface for reading and manipulating
/// WEBC container files. It supports multiple versions of WEBC container
/// formats and abstracts the underlying differences between them.

#[derive(Debug, Clone)]
pub struct Container {
    imp: Arc<dyn AbstractWebc + Send + Sync>,
}

impl Container {
    /// Load a [`Container`] from disk.
    ///
    /// Where possible, this will try to use a memory-mapped implementation
    /// to reduce memory usage.
    pub fn from_disk(path: impl AsRef<Path>) -> Result<Self, ContainerError> {
        let path = path.as_ref();
        let mut f = File::open(path).map_err(|error| ContainerError::Open {
            error,
            path: path.to_path_buf(),
        })?;

        match crate::detect(&mut f) {
            #[cfg(feature = "mmap")]
            Ok(Version::V1) => {
                let options = crate::v1::ParseOptions::default();
                let webc = crate::v1::WebCMmap::from_file(f, &options)?;
                Ok(Container::new(webc))
            }
            Ok(other) => {
                // fall back to the allocating version
                let mut buffer = Vec::new();
                f.rewind()
                    .and_then(|_| f.read_to_end(&mut buffer))
                    .map_err(|error| ContainerError::Read {
                        path: path.to_path_buf(),
                        error,
                    })?;

                Container::from_bytes_and_version(buffer.into(), other)
            }
            Err(e) => Err(ContainerError::Detect(e)),
        }
    }

    /// Load a [`Container`] from bytes in memory.
    pub fn from_bytes(bytes: impl Into<Bytes>) -> Result<Self, ContainerError> {
        let bytes: Bytes = bytes.into();
        let version = crate::detect(bytes.as_ref())?;
        Container::from_bytes_and_version(bytes, version)
    }

    fn new(repr: impl AbstractWebc + Send + Sync + 'static) -> Self {
        Container {
            imp: Arc::new(repr),
        }
    }

    fn from_bytes_and_version(bytes: Bytes, version: Version) -> Result<Self, ContainerError> {
        match version {
            Version::V1 => {
                let options = crate::v1::ParseOptions::default();
                let webc = crate::v1::WebCOwned::parse(bytes, &options)?;
                Ok(Container::new(webc))
            }
            Version::V2 => {
                let reader = crate::v2::read::OwnedReader::parse(bytes)?;
                Ok(Container::new(reader))
            }
            other => Err(ContainerError::UnsupportedVersion(other)),
        }
    }

    /// Get the [`Container`]'s manifest.

    pub fn manifest(&self) -> &crate::metadata::Manifest {
        self.imp.manifest()
    }

    /// Get all atoms stored in the container as a map.
    pub fn atoms(&self) -> BTreeMap<String, SharedBytes> {
        let mut atoms = BTreeMap::new();

        for name in self.imp.atom_names() {
            if let Some(atom) = self.imp.get_atom(&name) {
                atoms.insert(name.into_owned(), atom);
            }
        }

        atoms
    }

    /// Get an atom with the given name.
    ///
    /// Returns `None` if the atom does not exist in the container.
    ///
    /// This operation is pretty cheap, typically just a dictionary lookup
    /// followed by reference count bump and some index math.
    pub fn get_atom(&self, name: &str) -> Option<SharedBytes> {
        self.imp.get_atom(name)
    }

    /// Get all volumes stored in the container.
    pub fn volumes(&self) -> BTreeMap<String, Volume> {
        let mut volumes = BTreeMap::new();

        for name in self.imp.volume_names() {
            if let Some(atom) = self.imp.get_volume(&name) {
                volumes.insert(name.into_owned(), atom);
            }
        }

        volumes
    }

    /// Get a volume with the given name.
    ///
    /// Returns `None` if the volume does not exist in the container.
    pub fn get_volume(&self, name: &str) -> Option<Volume> {
        self.imp.get_volume(name)
    }
}

trait AbstractWebc: Debug {
    fn manifest(&self) -> &crate::metadata::Manifest;

    fn atom_names(&self) -> Vec<Cow<'_, str>>;
    fn get_atom(&self, name: &str) -> Option<SharedBytes>;
    fn volume_names(&self) -> Vec<Cow<'_, str>>;
    fn get_volume(&self, name: &str) -> Option<Volume>;
}

impl AbstractWebc for crate::v2::read::OwnedReader {
    fn manifest(&self) -> &crate::metadata::Manifest {
        self.manifest()
    }

    fn atom_names(&self) -> Vec<Cow<'_, str>> {
        self.atom_names().map(Cow::Borrowed).collect()
    }

    fn get_atom(&self, name: &str) -> Option<SharedBytes> {
        self.get_atom(name).cloned().map(SharedBytes::from)
    }

    fn volume_names(&self) -> Vec<Cow<'_, str>> {
        self.atom_names().map(Cow::Borrowed).collect()
    }

    fn get_volume(&self, name: &str) -> Option<Volume> {
        self.get_volume(name).ok().map(Volume::new)
    }
}

#[cfg(feature = "mmap")]
impl AbstractWebc for crate::v1::WebCMmap {
    fn manifest(&self) -> &crate::metadata::Manifest {
        &self.manifest
    }

    fn atom_names(&self) -> Vec<Cow<'_, str>> {
        self.get_all_atoms().into_keys().map(Cow::Owned).collect()
    }

    fn get_atom(&self, name: &str) -> Option<SharedBytes> {
        let atoms = self.get_all_atoms();
        let atom = atoms.get(name)?;
        let range = super::subslice_offsets(&self.state, atom);
        Some(SharedBytes::mapped(Arc::clone(&self.state), range))
    }

    fn volume_names(&self) -> Vec<Cow<'_, str>> {
        self.volumes
            .keys()
            .map(|s| Cow::Borrowed(s.as_str()))
            .collect()
    }

    fn get_volume(&self, name: &str) -> Option<Volume> {
        let package_name = self.get_package_name();
        let volume = WebC::get_volume(self, &package_name, name)?;

        Some(Volume::new(crate::compat::volume::VolumeV1::Mapped {
            state: Arc::clone(&self.state),
            volume: volume.clone(),
        }))
    }
}

impl AbstractWebc for crate::v1::WebCOwned {
    fn manifest(&self) -> &crate::metadata::Manifest {
        &self.manifest
    }

    fn atom_names(&self) -> Vec<Cow<'_, str>> {
        self.get_all_atoms().into_keys().map(Cow::Owned).collect()
    }

    fn get_atom(&self, name: &str) -> Option<SharedBytes> {
        let atoms = self.get_all_atoms();
        let atom = atoms.get(name)?;
        let range = super::subslice_offsets(&self.backing_data, atom);
        Some(self.backing_data.slice(range).into())
    }

    fn volume_names(&self) -> Vec<Cow<'_, str>> {
        self.volumes
            .keys()
            .map(|s| Cow::Borrowed(s.as_str()))
            .collect()
    }

    fn get_volume(&self, name: &str) -> Option<Volume> {
        let package_name = self.get_package_name();
        let volume = WebC::get_volume(self, &package_name, name)?;

        Some(Volume::new(crate::compat::volume::VolumeV1::Owned {
            bytes: self.backing_data.clone(),
            volume: volume.clone(),
        }))
    }
}

/// Various errors that may occur during [`Container`] operations.
#[derive(Debug, thiserror::Error)]
#[non_exhaustive]
pub enum ContainerError {
    /// Unable to detect the WEBC version.
    #[error("Unable to detect the WEBC version")]
    Detect(#[from] crate::DetectError),
    /// An unsupported WEBC version was found.
    #[error("Unsupported WEBC version, {_0}")]
    UnsupportedVersion(crate::Version),
    /// An error occurred while parsing a v1 WEBC file.
    #[error(transparent)]
    V1(#[from] crate::v1::Error),
    /// An error occurred while parsing a v2 WEBC file.
    #[error(transparent)]
    V2Owned(#[from] crate::v2::read::OwnedReaderError),
    /// Unable to open a file.
    #[error("Unable to open \"{}\"", path.display())]
    Open {
        /// The file's path.
        path: std::path::PathBuf,
        /// The underlying error.
        #[source]
        error: std::io::Error,
    },
    /// Unable to read a file's contents into memory.
    #[error("Unable to read \"{}\"", path.display())]
    Read {
        /// The file's path.
        path: std::path::PathBuf,
        /// The underlying error.
        #[source]
        error: std::io::Error,
    },
}
