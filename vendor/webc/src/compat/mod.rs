//! A compatibility layer for dealing with different versions of the WEBC binary
//! format.
//!
//! The [`Container`] provides an abstraction over the various WEBC versions
//! this crate can handle. As such, it tries to cater to the lowest common
//! denominator and favors portability over performance or power.
//!
//! In general, errors that occur during lazy operations won't be accessible to
//! the user.
//!
//! If more flexibility is required, consider using [`crate::detect()`] and
//! instantiating a compatible parser directly.
//!
//! ```rust,no_run
//! use webc::{
//!     compat::Container,
//!     Version,
//!     v2::read::OwnedReader,
//!     v1::{ParseOptions, WebC},
//! };
//! let bytes: &[u8] = b"...";
//!
//! match webc::detect(bytes) {
//!     Ok(Version::V1) => {
//!         let options = ParseOptions::default();
//!         let webc = WebC::parse(bytes, &options).unwrap();
//!         if let Some(signature) = webc.signature {
//!             println!("Package signature: {:?}", signature);
//!         }
//!     }
//!     Ok(Version::V2) => {
//!         let webc = OwnedReader::parse(bytes).unwrap();
//!         let index = webc.index();
//!         let signature = &index.signature;
//!         if !signature.is_none() {
//!             println!("Package signature: {signature:?}");
//!         }
//!     }
//!     Ok(other) => todo!("Unsupported version, {other}"),
//!     Err(e) => todo!("An error occurred: {e}"),
//! }
//! ```

mod container;
mod shared_bytes;
mod volume;

pub use self::{
    container::{Container, ContainerError},
    shared_bytes::SharedBytes,
    volume::{Metadata, Volume, VolumeError},
};

/// Find the offset of a `needle` within a large `haystack` allocation.
///
/// # Panics
///
/// This will panic if the needle lies outside of the allocation.
pub(crate) fn subslice_offsets(haystack: &[u8], needle: &[u8]) -> std::ops::Range<usize> {
    let haystack_range = haystack.as_ptr_range();
    let needle_range = needle.as_ptr_range();

    assert!(
        haystack_range.start <= needle_range.start && needle_range.end <= haystack_range.end,
        "expected {needle_range:?} to lie within {haystack_range:?}",
    );

    let start = (needle_range.start as usize)
        .checked_sub(haystack_range.start as usize)
        .expect("Needle out of range");
    let end = (needle_range.end as usize)
        .checked_sub(haystack_range.start as usize)
        .expect("Needle out of range");

    start..end
}
