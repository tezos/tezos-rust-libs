use std::{fmt::Debug, sync::Arc};

use bytes::Bytes;

use crate::{compat::SharedBytes, PathSegment, PathSegmentError, PathSegments, ToPathSegments};

/// A WEBC volume.
///
/// A `Volume` represents a collection of files and directories, providing
/// methods to read file contents and traverse directories.
///
/// # Example
///
/// ```
/// use webc::compat::{Metadata, Volume};
/// # use webc::{
/// #     compat::Container,
/// #     v2::{
/// #         write::{Directory, Writer},
/// #         read::OwnedReader,
/// #         SignatureAlgorithm,
/// #         PathSegment,
/// #     },
/// # };
///
/// fn get_webc_volume() -> Volume {
///     /* ... */
/// # let dir = Directory {
/// #   children: [
/// #       (
/// #           PathSegment::parse("path").unwrap(),
/// #           Directory {
/// #               children: [
/// #                   (
/// #                       PathSegment::parse("to").unwrap(),
/// #                       Directory {
/// #                           children: [
/// #                               (PathSegment::parse("file.txt").unwrap(), b"Hello, World!".to_vec().into()),
/// #                           ].into_iter().collect(),
/// #                       }.into(),
/// #                   ),
/// #               ].into_iter().collect(),
/// #           }.into(),
/// #       ),
/// #       (
/// #           PathSegment::parse("another.txt").unwrap(),
/// #           b"Another".to_vec().into(),
/// #       ),
/// #   ].into_iter().collect(),
/// # };
/// # let serialized = Writer::default()
/// #     .write_manifest(&webc::metadata::Manifest::default()).unwrap()
/// #     .write_atoms(std::collections::BTreeMap::new()).unwrap()
/// #     .with_volume("my_volume", dir).unwrap()
/// #     .finish(SignatureAlgorithm::None).unwrap();
/// #     Container::from_bytes(serialized).unwrap().get_volume("my_volume").unwrap()
/// }
///
/// let volume = get_webc_volume();
/// // Accessing file content.
/// let content = volume.read_file("/path/to/file.txt").unwrap();
/// assert_eq!(content, b"Hello, World!");
///
/// // Inspect directories.
/// let entries = volume.read_dir("/").unwrap();
/// assert_eq!(entries.len(), 2);
/// assert_eq!(entries[0], (
///     PathSegment::parse("another.txt").unwrap(),
///     Metadata::File { length: 7 },
/// ));
/// assert_eq!(entries[1], (
///     PathSegment::parse("path").unwrap(),
///     Metadata::Dir,
/// ));
/// ```
#[derive(Debug, Clone)]
pub struct Volume {
    imp: Arc<dyn AbstractVolume + Send + Sync + 'static>,
}

impl Volume {
    pub(crate) fn new(volume: impl AbstractVolume + Send + Sync + 'static) -> Self {
        Volume {
            imp: Arc::new(volume),
        }
    }

    /// Get the metadata of an item at the given path.
    ///
    /// Returns `None` if the item does not exist in the volume or an internal
    /// error occurred.
    pub fn metadata(&self, path: impl ToPathSegments) -> Option<Metadata> {
        let path = path.to_path_segments().ok()?;
        self.imp.metadata(&path)
    }

    /// Read the contents of a directory at the given path.
    ///
    /// Returns a vector of directory entries, including their metadata, if the
    /// path is a directory.
    ///
    /// Returns `None` if the path does not exist or is not a directory.
    pub fn read_dir(&self, path: impl ToPathSegments) -> Option<Vec<(PathSegment, Metadata)>> {
        let path = path.to_path_segments().ok()?;
        self.imp.read_dir(&path)
    }

    /// Read the contents of a file at the given path.
    ///
    /// Returns `None` if the path is not valid or the file is not found.
    pub fn read_file(&self, path: impl ToPathSegments) -> Option<SharedBytes> {
        let path = path.to_path_segments().ok()?;
        self.imp.read_file(&path)
    }
}

/// Metadata describing the properties of a file or directory.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Metadata {
    /// A directory.
    Dir,
    /// A file with a specified length.
    File {
        /// The number of bytes in this file.
        length: usize,
    },
}

impl Metadata {
    /// Returns `true` if the metadata represents a directory.
    pub fn is_dir(self) -> bool {
        matches!(self, Metadata::Dir)
    }

    /// Returns `true` if the metadata represents a file.
    pub fn is_file(self) -> bool {
        matches!(self, Metadata::File { .. })
    }
}

// TODO: This will probably need redesigning later on to make `DirEntry`
// "remember" where it is within a directory structure. The current design
//  turns directory walking into an O(n²) operation.
pub(crate) trait AbstractVolume: Debug {
    fn metadata(&self, path: &PathSegments) -> Option<Metadata>;
    fn read_dir(&self, path: &PathSegments) -> Option<Vec<(PathSegment, Metadata)>>;
    fn read_file(&self, path: &PathSegments) -> Option<SharedBytes>;
}

impl AbstractVolume for crate::v2::read::VolumeSection {
    fn metadata(&self, path: &PathSegments) -> Option<Metadata> {
        let entry = self.header().find(path).ok().flatten()?;
        Some(v2_metadata(&entry))
    }

    fn read_dir(&self, path: &PathSegments) -> Option<Vec<(PathSegment, Metadata)>> {
        let meta = self
            .header()
            .find(path)
            .ok()
            .flatten()
            .and_then(|entry| entry.into_dir())?;

        let mut entries = Vec::new();

        for (name, entry) in meta.entries().flatten() {
            let segment: PathSegment = name.parse().unwrap();
            let meta = v2_metadata(&entry);
            entries.push((segment, meta));
        }

        Some(entries)
    }

    fn read_file(&self, path: &PathSegments) -> Option<SharedBytes> {
        self.lookup_file(path).map(Into::into).ok()
    }
}

fn v2_metadata(header_entry: &crate::v2::read::volume_header::HeaderEntry<'_>) -> Metadata {
    match header_entry {
        crate::v2::read::volume_header::HeaderEntry::Directory(_) => Metadata::Dir,
        crate::v2::read::volume_header::HeaderEntry::File(
            crate::v2::read::volume_header::FileMetadata {
                start_offset,
                end_offset,
                ..
            },
        ) => Metadata::File {
            length: end_offset - start_offset,
        },
    }
}

#[derive(Debug)]
pub(crate) enum VolumeV1 {
    Owned {
        // SAFETY: order is important here because volume has references into
        // bytes.
        volume: crate::v1::Volume<'static>,
        bytes: Bytes,
    },
    #[cfg(feature = "mmap")]
    Mapped {
        // SAFETY: order is important here because volume has references into
        // state.
        volume: crate::v1::Volume<'static>,
        state: Arc<crate::SharedMmapState>,
    },
}

impl VolumeV1 {
    fn volume(&self) -> &crate::v1::Volume<'static> {
        match self {
            VolumeV1::Owned { volume, .. } => volume,
            #[cfg(feature = "mmap")]
            VolumeV1::Mapped { volume, .. } => volume,
        }
    }

    fn get_shared(&self, needle: &[u8]) -> SharedBytes {
        match self {
            VolumeV1::Owned { bytes, .. } => {
                let range = super::subslice_offsets(bytes, needle);
                bytes.slice(range).into()
            }
            #[cfg(feature = "mmap")]
            VolumeV1::Mapped { state, .. } => {
                let range = super::subslice_offsets(state, needle);
                SharedBytes::mapped(Arc::clone(state), range)
            }
        }
    }
}

impl AbstractVolume for VolumeV1 {
    fn metadata(&self, path: &PathSegments) -> Option<Metadata> {
        let path = path.to_string();

        if let Ok(bytes) = self.volume().get_file(&path) {
            return Some(Metadata::File {
                length: bytes.len(),
            });
        }

        if self.volume().read_dir(&path).is_ok() {
            return Some(Metadata::Dir);
        }

        None
    }

    fn read_dir(&self, path: &PathSegments) -> Option<Vec<(PathSegment, Metadata)>> {
        let path = path.to_string();

        let mut entries = Vec::new();

        for entry in self.volume().read_dir(&path).ok()? {
            let name: PathSegment = match entry.text.parse() {
                Ok(p) => p,
                Err(_) => continue,
            };
            let meta = v1_metadata(&entry);
            entries.push((name, meta));
        }

        Some(entries)
    }

    fn read_file(&self, path: &PathSegments) -> Option<SharedBytes> {
        let path = path.to_string();
        let bytes = self.volume().get_file(&path).ok()?;

        Some(self.get_shared(bytes))
    }
}

fn v1_metadata(entry: &crate::v1::FsEntry<'_>) -> Metadata {
    match entry.fs_type {
        crate::v1::FsEntryType::File => Metadata::File {
            length: entry.get_len().try_into().unwrap(),
        },
        crate::v1::FsEntryType::Dir => Metadata::Dir,
    }
}

/// Errors that may occur when doing [`Volume`] operations.
#[derive(Debug, Clone, PartialEq, thiserror::Error)]
pub enum VolumeError {
    /// The item wasn't found.
    #[error("The item wasn't found")]
    NotFound,
    /// The provided path wasn't valid.
    #[error("Invalid path")]
    Path(#[from] PathSegmentError),
    /// A non-directory was found where a directory was expected.
    #[error("Not a directory")]
    NotADirectory,
    /// A non-file was found where a file was expected.
    #[error("Not a file")]
    NotAFile,
}

#[cfg(test)]
mod tests {
    use std::collections::BTreeMap;

    use super::*;
    use crate::{metadata::Manifest, v1::DirOrFile, v2::write::Writer};

    fn v2_volume(volume: crate::v2::write::Directory<'static>) -> crate::v2::read::VolumeSection {
        let manifest = Manifest::default();
        let mut writer = Writer::default()
            .write_manifest(&manifest)
            .unwrap()
            .write_atoms(BTreeMap::new())
            .unwrap();
        writer.write_volume("volume", volume).unwrap();
        let serialized = writer.finish(crate::v2::SignatureAlgorithm::None).unwrap();
        let reader = crate::v2::read::OwnedReader::parse(serialized).unwrap();
        reader.get_volume("volume").unwrap()
    }

    #[test]
    fn v2() {
        let dir = dir_map! {
            "path" => dir_map! {
                "to" => dir_map! {
                    "file.txt" => b"Hello, World!",
                }
            },
            "another.txt" => b"Another",
        };
        let volume = v2_volume(dir);

        let volume = Volume::new(volume);

        assert!(volume.read_file("").is_none());
        assert_eq!(volume.read_file("/another.txt").unwrap(), b"Another");
        assert_eq!(
            volume.metadata("/another.txt").unwrap(),
            Metadata::File { length: 7 }
        );
        assert_eq!(
            volume.read_file("/path/to/file.txt").unwrap(),
            b"Hello, World!",
        );
        assert_eq!(
            volume.read_dir("/").unwrap(),
            vec![
                (
                    PathSegment::parse("another.txt").unwrap(),
                    Metadata::File { length: 7 },
                ),
                (PathSegment::parse("path").unwrap(), Metadata::Dir),
            ],
        );
        assert_eq!(
            volume.read_dir("/path").unwrap(),
            vec![(PathSegment::parse("to").unwrap(), Metadata::Dir)],
        );
        assert_eq!(
            volume.read_dir("/path/to/").unwrap(),
            vec![(
                PathSegment::parse("file.txt").unwrap(),
                Metadata::File { length: 13 }
            )],
        );
    }

    fn owned_volume_v1(entries: BTreeMap<DirOrFile, Vec<u8>>) -> VolumeV1 {
        let bytes: Bytes = crate::v1::Volume::serialize_files(entries).into();

        let borrowed_volume = crate::v1::Volume::parse(&bytes).unwrap();
        VolumeV1::Owned {
            // SAFETY: We need to transmute the lifetime away.
            volume: unsafe { std::mem::transmute(borrowed_volume) },
            bytes,
        }
    }

    #[test]
    fn v1_owned() {
        let mut dir = BTreeMap::new();
        dir.insert(DirOrFile::Dir("/path".into()), Vec::new());
        dir.insert(DirOrFile::Dir("/path/to".into()), Vec::new());
        dir.insert(
            DirOrFile::File("/path/to/file.txt".into()),
            b"Hello, World!".to_vec(),
        );
        dir.insert(DirOrFile::Dir("/path/to".into()), Vec::new());
        dir.insert(DirOrFile::File("/another.txt".into()), b"Another".to_vec());
        let volume = owned_volume_v1(dir);

        let volume = Volume::new(volume);

        assert!(volume.read_file("").is_none());
        assert_eq!(volume.read_file("/another.txt").unwrap(), b"Another");
        assert_eq!(
            volume.metadata("/another.txt").unwrap(),
            Metadata::File { length: 7 }
        );
        assert_eq!(
            volume.read_file("/path/to/file.txt").unwrap(),
            b"Hello, World!",
        );

        assert_eq!(volume.metadata("/path/to").unwrap(), Metadata::Dir,);
        assert_eq!(
            volume.read_dir("/").unwrap(),
            vec![
                (
                    PathSegment::parse("another.txt").unwrap(),
                    Metadata::File { length: 7 }
                ),
                (PathSegment::parse("path").unwrap(), Metadata::Dir),
            ],
        );
        assert_eq!(
            volume.read_dir("/path").unwrap(),
            vec![(PathSegment::parse("to").unwrap(), Metadata::Dir)],
        );
        assert_eq!(
            volume.read_dir("/path/to/").unwrap(),
            vec![(
                PathSegment::parse("file.txt").unwrap(),
                Metadata::File { length: 13 }
            )],
        );
    }
}
