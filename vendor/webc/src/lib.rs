//! A library for reading and writing WEBC files.

#![warn(unreachable_pub, elided_lifetimes_in_paths)]
#![deny(missing_docs, missing_debug_implementations)]

pub extern crate bytes;

#[cfg(test)]
#[macro_use]
extern crate pretty_assertions;

#[cfg(test)]
#[macro_use]
mod macros;
#[cfg(test)]
mod utils;

pub mod compat;
mod detect;
pub mod metadata;
#[cfg(feature = "mmap")]
mod mmap;
mod path_segments;
#[allow(missing_docs)]
pub mod v1;
pub mod v2;
mod version;

/// The type for [`MAGIC`].
pub type Magic = [u8; 5];

/// File identification bytes stored at the beginning of the file.
pub const MAGIC: Magic = *b"\0webc";

pub use crate::{
    compat::Container,
    detect::{detect, is_webc, DetectError},
    path_segments::{PathSegment, PathSegmentError, PathSegments, ToPathSegments},
    version::Version,
};

#[cfg(feature = "mmap")]
pub(crate) use crate::mmap::SharedMmapState;
