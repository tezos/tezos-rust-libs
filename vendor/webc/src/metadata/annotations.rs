//! Strongly-typed definitions for the various free-form "annotations" fields
//! in a [`crate::metadata::Manifest`].

/// The base URI used by a [`Wasi`] runner.
pub const WASI_RUNNER_URI: &str = "https://webc.org/runner/wasi";

/// The base URI used by a [`Wcgi`] runner.
pub const WCGI_RUNNER_URI: &str = "https://webc.org/runner/wcgi";

/// The base URI used by an [`Emscripten`] runner.
pub const EMSCRIPTEN_RUNNER_URI: &str = "https://webc.org/runner/emscripten";

/// The base URI used by a [WASM4] runner.
///
/// [WASM4]: https://wasm4.org/
pub const WASM4_RUNNER_URI: &str = "https://webc.org/runner/wasm4";

/// Annotations used by WASI runners.
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "camelCase")]
#[non_exhaustive]
pub struct Wasi {
    pub atom: String,
    #[serde(default)]
    pub package: Option<String>,
    #[serde(default)]
    pub env: Option<Vec<String>>,
    #[serde(default)]
    pub main_args: Option<Vec<String>>,
    #[serde(default)]
    pub mount_atom_in_volume: Option<String>,
}

impl Wasi {
    pub fn new(atom: impl Into<String>) -> Self {
        Wasi {
            atom: atom.into(),
            package: None,
            env: None,
            main_args: None,
            mount_atom_in_volume: None,
        }
    }
}

/// Annotations used by WCGI runners.
#[derive(Default, Debug, Clone, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "camelCase")]
#[non_exhaustive]
pub struct Wcgi {
    pub dialect: Option<String>,
}

/// Package-level annotations specific to the WebAssembly Package Manager.
#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
#[non_exhaustive]
pub struct Wapm {
    pub name: String,
    pub version: String,
    pub description: String,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub license: Option<String>,
    #[serde(
        rename = "license-file",
        default,
        skip_serializing_if = "Option::is_none"
    )]
    pub license_file: Option<VolumeSpecificPath>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub readme: Option<VolumeSpecificPath>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub repository: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub homepage: Option<String>,
}

impl Wapm {
    pub fn new(
        name: impl Into<String>,
        version: impl Into<String>,
        description: impl Into<String>,
    ) -> Self {
        Wapm {
            name: name.into(),
            version: version.into(),
            description: description.into(),
            license: None,
            license_file: None,
            readme: None,
            repository: None,
            homepage: None,
        }
    }
}

/// The path for an item inside a particular volume.
#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
pub struct VolumeSpecificPath {
    pub volume: String,
    pub path: String,
}

/// Annotations used by Emscripten runners.
#[derive(Default, Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Emscripten {
    #[serde(default)]
    pub atom: Option<String>,
    #[serde(default)]
    pub package: Option<String>,
    #[serde(default)]
    pub env: Option<Vec<String>>,
    #[serde(default)]
    pub main_args: Option<Vec<String>>,
    #[serde(default, rename = "mountAtomInVolume")]
    pub mount_atom_in_volume: Option<String>,
}
