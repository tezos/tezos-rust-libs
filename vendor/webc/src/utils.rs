use sha2::{Digest, Sha256};

pub(crate) fn length_field(value: impl AsRef<[u8]>) -> [u8; 8] {
    let length: u64 = value.as_ref().len().try_into().unwrap();
    length.to_le_bytes()
}

pub(crate) fn sha256(data: impl AsRef<[u8]>) -> [u8; 32] {
    let mut state = Sha256::default();
    state.update(data.as_ref());
    state.finalize().into()
}
