//! Read files in the WEBC format.

mod decoder;
mod dir_entry;
mod owned;
mod scanner;
mod sections;
mod streaming;
pub(crate) mod volume_header;

pub use self::{
    dir_entry::{DirEntry, Directory, FileEntry},
    owned::{OwnedReader, OwnedReaderError},
    sections::{
        AtomsSection, IndexSection, LookupError, ManifestSection, Section, SectionError,
        VolumeSection,
    },
    streaming::{StreamingReader, StreamingReaderError},
    volume_header::VolumeHeaderError,
};
