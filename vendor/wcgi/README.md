# wcgi

Tooling for running web servers on Wasmer Deploy.

Check out the [Tutorial](./docs/Tutorial.md) to get started.

## Repository layout

* [crates](./crates):
  Contains wcgi tooling for the Rust language, including a client library `wcgi`,
  a local wcgi runner and Rust examples.
  - [wcgi](./crates/wcgi)
    wcgi client library for easily writing wcgi handlers in Rust.
  - [wcgi_runner](./crates/wcgi_runner)
    wcgi runner for running a wcgi server locally.
  - [xtask](./crates/xtask)
    CLI with utility commands for managing this repository.
- [examples](./examples)
  Collection of example wcgi handlers written in Rust.

## Develop

### xtask

The `cargo xtask` command provides various management commands, like running
tests, publishing all examples, ...

Execute `cargo xtask` in your shell to see available commands.

**NOTE:** can only be run when in the root directory.
